#ifndef UNICODE
#define UNICODE
#endif

#define WIN32_LEAN_AND_MEAN

#include <iostream>
#include <ctime>

#include <winsock2.h>
#include <Ws2tcpip.h>
#include <stdio.h>

// Link with ws2_32.lib
#pragma comment(lib, "Ws2_32.lib")

#define DEFAULT_BUFLEN 62

#define IP_ADDRESS "192.168.4.1"
#define TELNET_PORT 2000

int main()
{
    //----------------------
    // Declare and initialize variables.
    int iResult;
    WSADATA wsaData;

    SOCKET ConnectSocket = INVALID_SOCKET;
    struct sockaddr_in clientService;
    
    int length = 0;
    std::clock_t start;

    int recvbuflen = DEFAULT_BUFLEN;
    char recvbuf[DEFAULT_BUFLEN] = "";
    char sendbuf[6];
    sendbuf[0] = 0x01;
    sendbuf[1] = 0xff;
    sendbuf[2] = 0x14;
    sendbuf[3] = 0x01;
    sendbuf[4] = 0x3b;
    sendbuf[5] = 0x04;
    
    //----------------------
    // Initialize Winsock
    iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
    if (iResult != NO_ERROR)
    {
        wprintf(L"WSAStartup failed with error: %d\n", iResult);
        return 1;
    }

    //----------------------
    // Create a SOCKET for connecting to server
    ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (ConnectSocket == INVALID_SOCKET)
    {
        wprintf(L"socket failed with error: %ld\n", WSAGetLastError());
        WSACleanup();
        return 1;
    }

    //----------------------
    // The sockaddr_in structure specifies the address family,
    // IP address, and port of the server to be connected to.
    clientService.sin_family = AF_INET;
    clientService.sin_addr.s_addr = inet_addr(IP_ADDRESS);
    clientService.sin_port = htons(TELNET_PORT);

    //----------------------
    // Connect to server.
    iResult = connect(ConnectSocket, (SOCKADDR *)&clientService, sizeof(clientService));
    if (iResult == SOCKET_ERROR)
    {
        wprintf(L"connect failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

    // Send an initial buffer
    iResult = send(ConnectSocket, sendbuf, (int)strlen(sendbuf), 0);
    if (iResult == SOCKET_ERROR)
    {
        printf("send failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }
	
	sendbuf[0] = 0x01;
    sendbuf[1] = 0x06;
    sendbuf[2] = 0x04;

	Sleep(100);
    start = std::clock();

    while (1)
    {
        iResult = recv(ConnectSocket, recvbuf, recvbuflen, 0);
        if (iResult > 0)
        {
            if (recvbuf[0] == 0x01 && (uint8_t)recvbuf[1] == 0xFE)
            {
                break;
            }
            else if (recvbuf[0] == 0x01 && recvbuf[1] == 0x70 && recvbuf[2] == 0x04)
            {
                std::cout << "Error received" << std::endl;
                break;
            }
			
            length += iResult-2;
			
		    iResult = closesocket(ConnectSocket);
		    ConnectSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
		    iResult = connect(ConnectSocket, (SOCKADDR *)&clientService, sizeof(clientService));
		    iResult = send(ConnectSocket, sendbuf, 3, 0);
        }
        else
            break;
    }

    double duration = (std::clock() - start) / (double)CLOCKS_PER_SEC;

    // shutdown the connection since no more data will be sent
    iResult = shutdown(ConnectSocket, SD_SEND);
    if (iResult == SOCKET_ERROR)
    {
        wprintf(L"shutdown failed with error: %d\n", WSAGetLastError());
        closesocket(ConnectSocket);
        WSACleanup();
        return 1;
    }

    // close the socket
    iResult = closesocket(ConnectSocket);
    if (iResult == SOCKET_ERROR)
    {
        wprintf(L"close failed with error: %d\n", WSAGetLastError());
        WSACleanup();
        return 1;
    }
	
    std::cout << "Length is:         " << length << std::endl;
    std::cout << "Duration is:   	 " << duration << std::endl;
    std::cout << "KiloBytes pr sec:  " << (length / duration)/1000 << std::endl;
    std::cout << "KiloBits pr sec:   " << ((length / duration) * 8)/1000 << std::endl;
	
	char wait;
	std::cin >> wait;
	
    WSACleanup();
    return 0;
}