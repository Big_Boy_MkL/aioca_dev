/**
 * C++ Libs
 */
#include <iostream>

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */
#include "nvs_flash.h"


/**
 * Private Headers
 */
#include "WifiController/WifiController.hpp"
#include "VideoStream/StreamCtrl.hpp"
#include "UDP/Socket.hpp"
#include "Tcp/Socket.hpp"
#include "TimeCheck/TimeCheck.hpp"
#include "GlobalDefines.hpp"

extern "C"
{
    void app_main(void);
}

WIFI::WifiController *ctrl;
VideoStream::StreamCtrl *stream;
UDP::ISocket *udp;
TCP::ISocket *tcp;
Camera::OV2640 *cam;
Utility::TimeCheck *timeCheck;

void app_main()
{
    // Setup internal flash storage needed in esp funtions
    ESP_ERROR_CHECK(nvs_flash_init());
    ESP_ERROR_CHECK(nvs_flash_erase());

    // Create Wifi controller
    ctrl = new WIFI::WifiController();

    // Setup and start WiFi
    ctrl->SetStaMode("AIOCA", "12345678", 254);
    ctrl->Start();

    // Create all needed classes 
    QueueHandle_t queue = xQueueCreate(TCPSOCKET_MSGQUEUE_SIZE, sizeof(TCP::MessageQueueData));
    udp = new UDP::Socket();
    tcp = new TCP::Socket(queue);
    cam = new Camera::OV2640();
    timeCheck = new Utility::TimeCheck(TIMER_GROUP_0, TIMER_0, 5);
    stream = new VideoStream::StreamCtrl(queue, udp, cam, timeCheck);

    // Start TCP Socket and stream
    tcp->Start();
    stream->Start();
}