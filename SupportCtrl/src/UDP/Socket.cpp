/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */
#include "lwip/sys.h"
#include <lwip/netdb.h>

/**
 * Private Headers
 */
#include "Socket.hpp"
#include "GlobalDefines.hpp"

namespace UDP
{
    Socket::Socket()
    {
        struct sockaddr_in sourceAddr;
        sourceAddr.sin_addr.s_addr = htonl(INADDR_ANY);
        sourceAddr.sin_family = AF_INET;
        sourceAddr.sin_port = htons(UDPSOCKET_SOCKETPORT);

        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
        if (sock < 0)
        {
        }
        int error = bind(sock, (struct sockaddr *)&sourceAddr, sizeof(sourceAddr));
        if (error < 0)
        {
        }
    }

    Socket::~Socket()
    {
        close(sock);
    }

    void Socket::Transmit(uint8_t *pictureBuffer, size_t *length, in_addr_t addr)
    {
        // Divide picture into small chucks so it can be sent over UDP
        size_t chunks = *length / size;
        size_t rest = *length % size;
        size_t error;

        // Create structure to hold client ip and port
        struct sockaddr_in distAddr;
        distAddr.sin_addr.s_addr = addr;
        distAddr.sin_family = AF_INET;
        distAddr.sin_port = htons(2000);
        
        // Send frame start + size
        size_t headerSize = 7;
        uint8_t header[headerSize] = {PROTOCOL_START, PROTOCOL_WRITE, (uint8_t)(*length >> 24) , (uint8_t)(*length >> 16), (uint8_t)(*length >> 8), (uint8_t)*length, PROTOCOL_END};
        error = sendto(sock, header, headerSize, 0, (struct sockaddr *)&distAddr, sizeof(distAddr));

        // Resend package if error occured
        ResendPackage(header, &headerSize, &error, &distAddr);

        // Start sending picture
        for (size_t i = 0; i < chunks; i++)
        {
            // Send UDP package
            error = sendto(sock, pictureBuffer + (i * size), size, 0, (struct sockaddr *)&distAddr, sizeof(distAddr));

            // Resend package if error occured
            ResendPackage(pictureBuffer + (i * size), &size, &error, &distAddr);
        }

        if (rest > 0)
        {
            // Send the last package
            error = sendto(sock, pictureBuffer + (chunks * size), rest, 0, (struct sockaddr *)&distAddr, sizeof(distAddr));

            // Resend last package if error occured
            ResendPackage(pictureBuffer + (chunks * size), &rest, &error, &distAddr);
        }
    }

    void Socket::ResendPackage(uint8_t *buffer, size_t *length, size_t *error, sockaddr_in *distAddr)
    {
        uint8_t counter = 0;

        // Keep resending package
        while (*error == -1)
        {
            // Packet was not send - Resend package after 2 ms
            vTaskDelay(2 / portTICK_PERIOD_MS);

            // Resend package
            *error = sendto(sock, buffer, *length, 0, (struct sockaddr *)distAddr, sizeof(*distAddr));

            if (counter++ > 20)
            {
                break;
            }   
        }
    }
} // namespace UDP