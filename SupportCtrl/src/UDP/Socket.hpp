#ifndef _UDPSOCKET_H_
#define _UDPSOCKET_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */
#include "lwip/sockets.h"

/**
 * Private Headers
 */
#include "ISocket.hpp"

namespace UDP
{
    class Socket : public ISocket
    {
    private:
        int sock;
        // Max size of one UDP packet
        size_t size = 6000;
        void ResendPackage(uint8_t *buffer, size_t *length, size_t *error, sockaddr_in *sourceAddr);

    public:
        Socket();
        ~Socket();
        void Transmit(uint8_t *pictureBuffer, size_t *length, in_addr_t addr);
    };
} // namespace UDP

#endif