#ifndef _IUDPSCOKET_H_
#define _IUDPSCOKET_H_

/**
 * C++ Libs
 */
#include <stdio.h>

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

/**
 * Private Headers
 */

namespace UDP
{
    class ISocket
    {
    public:
        virtual void Transmit(uint8_t *pictureBuffer, size_t *length, in_addr_t addr) = 0;
    };
} // namespace UDP
#endif