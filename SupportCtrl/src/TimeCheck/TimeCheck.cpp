/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */


/**
 * Private Headers
 */
#include "TimeCheck.hpp"

namespace Utility
{
    TimeCheck::TimeCheck(timer_group_t group, timer_idx_t index, double timeOut) : group_(group), index_(index), timeOut_(timeOut)
    {
        // Create configuration struct
        timer_config_t config = {
            .alarm_en = TIMER_ALARM_DIS,
            .counter_en = TIMER_START,
            .intr_type = TIMER_INTR_LEVEL,
            .counter_dir = TIMER_COUNT_UP,
            .auto_reload = TIMER_AUTORELOAD_DIS,
            .divider = 65536,
        };

        // Initilize timer
        ESP_ERROR_CHECK(timer_init(group, index, &config));
        ESP_ERROR_CHECK(timer_set_counter_value(group_, index_, 0xffff));
    }

    /**************************************************************************************************/

    TimeCheck::~TimeCheck()
    {
        timer_deinit(group_, index_);
    }

    /**************************************************************************************************/

    bool TimeCheck::CheckTimeout()
    {
        double currentTime = timeOut_;
        // Read timer in seconds put time into data
        ESP_ERROR_CHECK(timer_get_counter_time_sec(group_, index_, &currentTime));

        if (currentTime >= timeOut_)
        {
            timer_pause(group_, index_);
            return true;
        }
        else
        {
            return false;
        }
    }

    void TimeCheck::FeedTimeCheck()
    {
        timer_set_counter_value(group_, index_, 0);
    }

    /**************************************************************************************************/

    void TimeCheck::Start()
    {
        timer_set_counter_value(group_, index_, 0);
        ESP_ERROR_CHECK(timer_start(group_, index_));
    }
}