// /**
//  * C++ Libs
//  */
// #include <iostream>

// /**
//  * FreeRTOS Libs
//  */
// #include "freertos/FreeRTOS.h"
// #include "freertos/task.h"

// /**
//  * ESP Libs
//  */
// #include "nvs_flash.h"

// /**
//  * Private Headers
//  */
// #include "WifiController/WifiController.hpp"
// #include "VideoStream/StreamCtrl.hpp"
// #include "Camera/OV2640.hpp"

// extern "C"
// {
//     void app_main(void);
// }

// void WifiTest()
// {
//     auto wifi = WIFI::WifiController();
    
//     wifi.SetStaMode("AIOCA", "12345678", 254);
//     wifi.Start();

//     while (1)
//     {
//         vTaskDelay(1000/portTICK_PERIOD_MS);
//     }
// }

// void CameraTest()
// {
//     auto cam = Camera::OV2640();

//     size_t len;
//     uint8_t *pictureBuf = cam.TakePicture(&len);
//     for (size_t i = 0; i < len; i++)
//     {
//         printf("%02X", pictureBuf[i]);
//     }
//     printf("\n");

//     while (1)
//     {
//         vTaskDelay(1000/portTICK_PERIOD_MS);
//     }
// }

// void UDPTest()
// {
//     auto cam = Camera::OV2640();
//     auto udp = UDP::Socket();

//     size_t len;
//     while (1)
//     {
//         uint8_t *pictureBuf = cam.TakePicture(&len);

//         struct sockaddr_in sa;
//         inet_pton(AF_INET, "192.1.4.2", &(sa.sin_addr));
//         udp.Transmit(pictureBuf, &len, sa.sin_addr.s_addr);
//         vTaskDelay(1000/portTICK_PERIOD_MS);
//     }
// }

// void app_main()
// {
//     // Setup internal flash storage needed in esp funtions
//     ESP_ERROR_CHECK(nvs_flash_init());
//     ESP_ERROR_CHECK(nvs_flash_erase());

//     // WifiTest();
//     CameraTest();
//     // UDPTest();
// }