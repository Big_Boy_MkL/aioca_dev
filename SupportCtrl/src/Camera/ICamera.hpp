#ifndef _ICAMERA_H_
#define _ICAMERA_H_

#include <stdint.h>

namespace Camera
{
    class ICamera
    {
    public:
        uint8_t *TakePicture(size_t *length);
        void ReturnPictureBuf();
    };
} // namespace OV2640

#endif