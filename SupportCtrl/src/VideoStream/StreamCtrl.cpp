/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include "StreamCtrl.hpp"
#include "GlobalDefines.hpp"
#include "VSControllerTaskWrappers.hpp"

namespace VideoStream
{
    StreamCtrl::StreamCtrl(QueueHandle_t queue, UDP::ISocket* udp, Camera::OV2640* cam, Utility::TimeCheck* time) : tcpQueue_(queue), udpSocket_(udp), camera_(cam), timeout_(time)
    {
    }

    StreamCtrl::~StreamCtrl()
    {
    }

    void StreamCtrl::Start()
    {
        // Create stream task
        xTaskCreate(StreamCtrlRunWrapper, "StreamTask", 8192, this, 5, runStreamHandle_);
    }

    void StreamCtrl::Stop()
    {
        if (runStreamHandle_ != NULL)
        {
            vTaskDelete(runStreamHandle_);
        }
    }

    void StreamCtrl::Run()
    {
        in_addr_t addr = 0;
        while (1)
        {
            // Check if message was received
            if (uxQueueMessagesWaiting(tcpQueue_) > 0)
            {
                // Check message
                addr = CheckMessage(tcpQueue_);
            }

            if (!timeout_->CheckTimeout())
            {
                // Create variable buffer length
                size_t length = 0;

                // Take picture
                uint8_t *picture = camera_->TakePicture(&length);

                // Transmit picture
                udpSocket_->Transmit(picture, &length, addr);

                // Return buffer for reuse
                camera_->ReturnPictureBuf();
            }
            else
            {
                // Feed watchdog
                vTaskDelay(10 / portTICK_PERIOD_MS);
            }
        }
    }

    in_addr_t StreamCtrl::CheckMessage(QueueHandle_t tcpQueue_)
    {
        // Create message struckture
        TCP::MessageQueueData data;

        // Receive data from queue
        if (xQueueReceive(tcpQueue_, &(data), (TickType_t)20) == pdPASS)
        {
            // Check if message is correct size and contains start and end
            if (data.len == 3 && data.message[0] == PROTOCOL_START && data.message[2] == PROTOCOL_END)
            {
                // Determine if stream should start or stop
                if (data.message[1] == 0xff)
                {
                    // Start timeout timer
                    timeout_->Start();

                    // Send Ack
                    uint8_t ack[3] = {PROTOCOL_START, PROTOCOL_ACK, PROTOCOL_END};
                    data.socketPtr->SendMessage(&data.sock, ack, 3, false);
                }                
                else if (data.message[1] == 0xfe)
                {
                    // Feed timer
                    timeout_->FeedTimeCheck();

                    // Send Ack
                    uint8_t ack[3] = {PROTOCOL_START, PROTOCOL_ACK, PROTOCOL_END};
                    data.socketPtr->SendMessage(&data.sock, ack, 3, false);
                }
            }
            else 
            {
                // Send Error
                data.socketPtr->SendError(&data.sock, PROTOCOL_NAK);
            }
        }

        // Return address
        return data.addr;
    }
} // namespace VideoStream