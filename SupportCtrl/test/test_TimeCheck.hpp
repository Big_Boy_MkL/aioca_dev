#ifndef _TEST_TIMECHECK_H_
#define _TEST_TIMECHECK_H_

#include <unity.h>
#include "TimeCheck/TimeCheck.hpp"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

void test_TimeCheck_CheckTimeout_NoTimeout()
{
    Utility::TimeCheck time = Utility::TimeCheck(TIMER_GROUP_1, TIMER_0, 1);

    time.Start();
    vTaskDelay(200/portTICK_PERIOD_MS);
    bool timeout = time.CheckTimeout();

    TEST_ASSERT_EQUAL(false, timeout);
}

void test_TimeCheck_CheckTimeout_Timeout()
{
    Utility::TimeCheck time = Utility::TimeCheck(TIMER_GROUP_1, TIMER_0, 1);

    time.Start();
    vTaskDelay(1200/portTICK_PERIOD_MS);
    bool timeout = time.CheckTimeout();

    TEST_ASSERT_EQUAL(true, timeout);
}

void test_TimeStamp_CheckTimeStamp_Feed_NoTimeout()
{
    Utility::TimeCheck timer = Utility::TimeCheck(TIMER_GROUP_1, TIMER_0, 1);

    timer.Start();
    vTaskDelay(500/portTICK_PERIOD_MS);
    timer.FeedTimeCheck();
    vTaskDelay(750/portTICK_PERIOD_MS);
    bool timeout = timer.CheckTimeout();

    TEST_ASSERT_EQUAL(false, timeout);
}

void test_TimeStamp_CheckTimeStamp_Feed_Timeout()
{
    Utility::TimeCheck timer = Utility::TimeCheck(TIMER_GROUP_1, TIMER_0, 1);

    timer.Start();
    vTaskDelay(500/portTICK_PERIOD_MS);
    timer.FeedTimeCheck();
    vTaskDelay(1200/portTICK_PERIOD_MS);
    bool timeout = timer.CheckTimeout();

    TEST_ASSERT_EQUAL(true, timeout);
}

void test_TimeCheck()
{
    RUN_TEST(test_TimeCheck_CheckTimeout_NoTimeout);
    RUN_TEST(test_TimeCheck_CheckTimeout_Timeout);
    RUN_TEST(test_TimeStamp_CheckTimeStamp_Feed_NoTimeout);
    RUN_TEST(test_TimeStamp_CheckTimeStamp_Feed_Timeout);
}

#endif