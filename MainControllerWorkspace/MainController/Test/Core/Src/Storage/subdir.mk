################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../Core/Src/Storage/SD.cpp \
../Core/Src/Storage/SettingsManager.cpp 

OBJS += \
./Core/Src/Storage/SD.o \
./Core/Src/Storage/SettingsManager.o 

CPP_DEPS += \
./Core/Src/Storage/SD.d \
./Core/Src/Storage/SettingsManager.d 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/Storage/SD.o: ../Core/Src/Storage/SD.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m7 -std=gnu++14 -g3 -DUSE_HAL_DRIVER '-DUSE_HAL_SMBUS_REGISTER_CALLBACKS=1' -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I../Core/Src -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Core/Src/Storage/SD.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/Storage/SettingsManager.o: ../Core/Src/Storage/SettingsManager.cpp
	arm-none-eabi-g++ "$<" -mcpu=cortex-m7 -std=gnu++14 -g3 -DUSE_HAL_DRIVER '-DUSE_HAL_SMBUS_REGISTER_CALLBACKS=1' -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I../Core/Src -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -fno-rtti -fno-threadsafe-statics -fno-use-cxa-atexit -Wall -fstack-usage -MMD -MP -MF"Core/Src/Storage/SettingsManager.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

