################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Core/Src/freertos.c \
../Core/Src/stm32h7xx_hal_msp.c \
../Core/Src/stm32h7xx_hal_timebase_tim.c \
../Core/Src/stm32h7xx_it.c \
../Core/Src/syscalls.c \
../Core/Src/sysmem.c \
../Core/Src/system_stm32h7xx.c 

C_DEPS += \
./Core/Src/freertos.d \
./Core/Src/stm32h7xx_hal_msp.d \
./Core/Src/stm32h7xx_hal_timebase_tim.d \
./Core/Src/stm32h7xx_it.d \
./Core/Src/syscalls.d \
./Core/Src/sysmem.d \
./Core/Src/system_stm32h7xx.d 

OBJS += \
./Core/Src/freertos.o \
./Core/Src/stm32h7xx_hal_msp.o \
./Core/Src/stm32h7xx_hal_timebase_tim.o \
./Core/Src/stm32h7xx_it.o \
./Core/Src/syscalls.o \
./Core/Src/sysmem.o \
./Core/Src/system_stm32h7xx.o 


# Each subdirectory must supply rules for building sources it contributes
Core/Src/freertos.o: ../Core/Src/freertos.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/freertos.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32h7xx_hal_msp.o: ../Core/Src/stm32h7xx_hal_msp.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32h7xx_hal_msp.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32h7xx_hal_timebase_tim.o: ../Core/Src/stm32h7xx_hal_timebase_tim.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32h7xx_hal_timebase_tim.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/stm32h7xx_it.o: ../Core/Src/stm32h7xx_it.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/stm32h7xx_it.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/syscalls.o: ../Core/Src/syscalls.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/syscalls.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/sysmem.o: ../Core/Src/sysmem.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/sysmem.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"
Core/Src/system_stm32h7xx.o: ../Core/Src/system_stm32h7xx.c
	arm-none-eabi-gcc "$<" -mcpu=cortex-m7 -std=gnu11 -g3 -DUSE_HAL_DRIVER -DSTM32H7A3xxQ -DDEBUG -DTESTING -c -I../Core/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc -I../Drivers/STM32H7xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32H7xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc/Hardware -I../Core/Inc/Camera -I../Core/Inc/Serial -I../Core/Inc/Util -I../Core/Inc/Camera/ImageUtil -I../Core/Inc/ESP32 -I../Core/Inc/BusErrorDetection -I../Core/Inc/Settings -I../Middlewares/Third_Party/FreeRTOS/Source/include -I../Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 -I../Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F -I../Core/Inc/Thread -I../FATFS/Target -I../FATFS/App -I../Middlewares/Third_Party/FatFs/src -I../Utilities/JPEG -I../Middlewares/Third_Party -I"C:/Users/SYFO/Documents/Bachelor/git/aioca_dev/MainControllerWorkspace/MainController/TestFolder/Mocks" -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -MMD -MP -MF"Core/Src/system_stm32h7xx.d" -MT"$@" --specs=nano_c_standard_cpp.specs -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -o "$@"

