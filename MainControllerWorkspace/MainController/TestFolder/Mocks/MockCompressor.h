/*
 * MockCompressor.h
 *
 *  Created on: Nov 17, 2020
 *      Author: SYFO
 */

#ifndef MOCKS_MOCKCOMPRESSOR_H_
#define MOCKS_MOCKCOMPRESSOR_H_

#include <Compression/ICompressor.h>

class MockCompressor : public Compression::ICompressor {
public:
	MockCompressor(uint32_t fileSize) : fileSize_(fileSize){}

	uint8_t* Compress(uint8_t* src){return nullptr;}

	uint8_t* GetCompressedFile(){return nullptr;}

	uint32_t GetFileSize(){return fileSize_;}

	virtual ~MockCompressor(){}
private:
	uint32_t fileSize_;
};

#endif /* MOCKS_MOCKCOMPRESSOR_H_ */
