/*
 * Communication.h
 *
 *  Created on: 17 Nov 2020
 *      Author: SYFO
 */

#ifndef MOCK_ESP32_MOCKCOMMUNICATION_H_
#define MOCK_ESP32_MOCKCOMMUNICATION_H_

#include <string.h>
#include <ESP32/ICommunication.h>

class MockCommunication : public ESP32::ICommunication{
public:
	MockCommunication(uint8_t* bufferPtr, uint8_t readResult = 0) :
		bufferPtr_(bufferPtr),
		readResult_(readResult),
		readCount_(0),
		writeCount_(0){}

	void WriteAsync(const uint8_t* response, uint32_t count){
		memcpy(bufferPtr_, response, count);
		writeCount_++;
	}

	void WriteSync(const uint8_t* response, uint32_t count, uint32_t timeout){
		memcpy(bufferPtr_, response, count);
		writeCount_++;
	}

	void ReadSync(uint8_t* recieved, uint32_t count, uint32_t timeout){
		memcpy(recieved,&readResult_,1);
		readCount_++;
	}

	// Mirrored methods from SPI
	void BeginReadAsync(uint8_t* data, const uint16_t count){
		memcpy(data,&readResult_,1);
		readCount_++;
	}

	bool MessageReady(){return true;}
	uint32_t GetMessageLength(){return 0;}
	void Reset(){}

	/* Error detection */
	bool Validate(uint8_t* msg, uint32_t count){return true;}

	uint32_t GetReadCount(){return readCount_;}
	uint32_t GetWriteCount(){return writeCount_;}

	virtual ~MockCommunication(){}
private:
	uint8_t* bufferPtr_;
	uint8_t readResult_;

	uint32_t readCount_;
	uint32_t writeCount_;

};

#endif /* MOCK_ESP32_MOCKCOMMUNICATION_H_ */
