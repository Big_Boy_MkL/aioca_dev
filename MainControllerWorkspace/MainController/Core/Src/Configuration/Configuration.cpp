/*
 * Configuration.cpp
 *
 *  Created on: Nov 5, 2020
 *      Author: SYFO
 */

#include <Configuration/Configuration.h>
#include <Thread/Mutex.hpp>

// Storage class
extern Storage::SD g_SDCard;

namespace Configuration {

Storage::SettingsManager g_SettingsManager(g_SDCard);

// User setting stuff
User::Account g_CurrentAccount(g_SettingsManager,Thread::Mutex::k_SettingsManagerMutex);

}
