/*
 * SD.cpp
 *
 *  Created on: 3 Nov 2020
 *      Author: SYFO
 */

#include <Storage/SD.h>
#include <Storage/Exceptions/SDExceptions.h>

namespace Storage {

SD::SD() : lineReading_(false){
}

unsigned int SD::GetNumberOfFilesInDir(const char* directory)
{
	// Inspiration from http://irtos.sourceforge.net/FAT32_ChaN/doc/en/readdir.html
	FRESULT fres;
	DIR dir;

	// Open the directory
	fres = f_opendir(&dir, directory);

	if (fres == FR_NO_PATH){
		f_mkdir(directory);
		fres = f_opendir(&dir, directory);
	}

	if (fres != FR_OK)
		throw OpenError();

	/* Count number of files */
	unsigned int count = 0;
	FILINFO fno;

	for (;;){
		fres = f_readdir(&dir, &fno);

		if (fres != FR_OK || fno.fname[0] == 0) break;  /* Break on error or end of dir */
		if (fno.fname[0] == '.') continue;             /* Ignore dot entry */

		if (!(fno.fattrib & AM_DIR)) {                    /* It is a file */
			count++;
		}
	}

	f_closedir(&dir);	// Close directory again!

	return count;
}

void SD::CreateDirectory(const char* directory)
{
	FRESULT fres;
	DIR dir;

	fres = f_mkdir(directory);

	if (fres!=FR_OK)
		throw SDException();

}


void SD::OpenDir(const char* directory)
{
	FRESULT fres;
	DIR dir;

	// Open the directory
	fres = f_opendir(&iteratorDir_, directory);

	if (fres == FR_NO_PATH)
		throw NoPathError();

	if (fres != FR_OK)
		throw OpenError();
}

SDIterator SD::begin()
{
	FRESULT fres;
	fres = f_readdir(&iteratorDir_, &iteratorFile_);

	if (fres != FR_OK)
		throw ReadError();

	return SDIterator(&iteratorFile_,&iteratorDir_);
}

SDIterator SD::end()
{
	return SDIterator(nullptr,nullptr);
}

unsigned int SD::WriteToFile(const char* fileName, uint8_t* buffer, uint32_t bufferLen)
{
	FRESULT fres; 	// Result after operations

	/* Open File */
	fres = f_open(&hFile_, fileName, FA_WRITE | FA_OPEN_ALWAYS | FA_CREATE_ALWAYS);
	if (fres == FR_NO_PATH){
		f_close(&hFile_);	// Close the file after use!
		throw NoPathError();
	}
	else if (fres!=FR_OK){
		f_close(&hFile_);	// Close the file after use!
		throw OpenError();	// TODO: Specify specific exception for specific err return values from http://irtos.sourceforge.net/FAT32_ChaN/doc/en/open.html
	}

	/* Write to file */
	unsigned int bytesWritten = 0;
	fres = f_write(&hFile_, buffer, bufferLen, &bytesWritten);
	if(fres != FR_OK) {
		throw WriteError();
	}

	f_close(&hFile_);	// Close the file after use!

	return bytesWritten;
}

void SD::AppendToFile(const char* fileName, uint8_t* buffer, uint32_t bufferLen)
{
	FRESULT fres; 	// Result after operations

	/* Open File as append */
	fres = f_open(&hFile_, fileName, FA_WRITE | FA_OPEN_APPEND);
	if (fres != FR_OK) {
		throw OpenError();	// TODO: Specify specific exception for specific err return values from http://irtos.sourceforge.net/FAT32_ChaN/doc/en/open.html
	}

	/* Write to file */
	unsigned int bytesWritten = 0;
	fres = f_write(&hFile_, buffer, bufferLen, &bytesWritten);
	if(fres != FR_OK) {
		throw WriteError();
	}

	f_close(&hFile_);	// Close the file after use!

}

bool SD::ReadLine(const char* fileName, uint8_t* buffer, uint32_t maxReadLen)
{
	FRESULT fres; 	// Result after operations

	/* Open File */
	if (!lineReading_){
		fres = f_open(&hFile_, fileName, FA_READ);
		if (fres != FR_OK) {
			return false;
		}
		lineReading_ = true;
	}

	/* Read from file */
	bool moreData = true;
	TCHAR* res = f_gets((TCHAR*)buffer, maxReadLen, &hFile_);

	if (res != (TCHAR*)buffer){
		moreData = false;
	}

	return moreData;
}

void SD::ResetRead()
{
	f_close(&hFile_);	// Close the file
	lineReading_ = false;
}

unsigned int SD::ReadFromFile(const char* fileName, uint8_t* buffer, uint32_t maxReadLen)
{
	FRESULT fres; 	// Result after operations

	/* Open File */
	fres = f_open(&hFile_, fileName, FA_READ);
	if (fres != FR_OK) {
		throw OpenError();	// TODO: Specify specific exception for specific err return values from http://irtos.sourceforge.net/FAT32_ChaN/doc/en/open.html
	}

	/* Read from file */
	unsigned int bytesRead = 0;
	fres = f_read(&hFile_, buffer, maxReadLen, &bytesRead);

	if (fres != FR_OK){
		throw ReadError();
	}

	f_close(&hFile_);	// Close the file after use!

	return bytesRead;
}

void SD::Initialize()
{
	/* Mount file system -> Registers workspace */
	FRESULT fres; 	// Result after operations

	fres = f_mount(&hFatFs_, "", 1);	// 1 = Mount immediately

	if (fres != FR_OK) {
		throw MountFailure();
	}

}

void SD::Deinitialize()
{
	/* Demount we're done! */
	f_mount(NULL, "", 0);
}


SD::~SD() {
	/* Demount we're done! */
	Deinitialize();
}
} /* namespace Storage */
