/*
 * Account.h
 *
 *  Created on: Nov 5, 2020
 *      Author: SYFO
 */

#ifndef INC_USER_ACCOUNT_H_
#define INC_USER_ACCOUNT_H_

#include <Storage/SettingsTypes.h>
#include <Storage/SettingsManager.h>
#include <cmsis_os2.h>

namespace User {

class Account {
public:
	Account(Storage::SettingsManager& setManager, osMutexId_t& mutex);

	void UpdateSettings();

	UserSettings_t FetchSettings(char* userID);

	void SetUserID(char* userID);

	UserSettings_t GetSettings();

	void SetSetting(unsigned int settingNumber, uint8_t settingVal);
	uint8_t GetSetting(unsigned int settingNumber);

	virtual ~Account();

private:
	UserSettingsUnion userSettings_;
	Storage::SettingsManager setManager_;

	char userID_[k_UserIDSize+1];
	osMutexId_t& mutex_;

};

} /* namespace User */

#endif /* INC_USER_ACCOUNT_H_ */
