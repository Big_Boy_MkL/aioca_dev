/*
 * Configuration.hpp
 *
 *  Created on: Nov 5, 2020
 *      Author: SYFO
 */

#ifndef INC_CONFIGURATION_CONFIGURATION_H_
#define INC_CONFIGURATION_CONFIGURATION_H_

#include <Storage/SD.h>
#include <Storage/SettingsManager.h>
#include <User/Account.h>

namespace Configuration {

// User setting stuff
extern Storage::SettingsManager g_SettingsManager;
extern User::Account g_CurrentAccount;

}

#endif /* INC_CONFIGURATION_CONFIGURATION_H_ */
