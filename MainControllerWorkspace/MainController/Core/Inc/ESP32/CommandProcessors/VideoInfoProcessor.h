/*
 * VideoInfoProcessor.h
 *
 *  Created on: Oct 12, 2020
 *      Author: SYFO
 */

#ifndef INC_ESP32_COMMANDPROCESSORS_VIDEOINFOPROCESSOR_H_
#define INC_ESP32_COMMANDPROCESSORS_VIDEOINFOPROCESSOR_H_

#include <ESP32/CommandProcessors/ICommandProcessor.h>
#include <Video/IVideoController.h>

namespace ESP32 {

class VideoInfoProcessor : public ICommandProcessor {
public:
	VideoInfoProcessor(Video::IVideoController& vidCtrl) : vidCtrl_(vidCtrl){}

	void operator()(ICommunication& communicator, Command& cmd)
	{
		uint8_t vidSpecifier = cmd.arguments[0];

		Video::uVideoInfoFile fileUnion;

		fileUnion.parsed = vidCtrl_.GetVideoInformation(vidSpecifier);

		communicator.WriteSync(fileUnion.raw, Video::k_VideoInfoFileSize, 10000);
	}

	uint8_t GetCode() const { return 0x12; }

	virtual ~VideoInfoProcessor(){}
private:
	Video::IVideoController& vidCtrl_;
};

} /* namespace ESP32 */

#endif /* INC_ESP32_COMMANDPROCESSORS_VIDEOINFOPROCESSOR_H_ */
