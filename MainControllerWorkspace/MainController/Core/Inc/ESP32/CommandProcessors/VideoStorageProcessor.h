/*
 * VideoInfoProcessor.h
 *
 *  Created on: Oct 12, 2020
 *      Author: SYFO
 */

#ifndef INC_ESP32_COMMANDPROCESSORS_VIDEOSTORAGEPROCESSOR_H_
#define INC_ESP32_COMMANDPROCESSORS_VIDEOSTORAGEPROCESSOR_H_

#include <ESP32/CommandProcessors/ICommandProcessor.h>
#include <Video/IVideoController.h>
#include <stdio.h>

namespace ESP32 {

class VideoStorageProcessor : public ICommandProcessor{
public:

	VideoStorageProcessor(Video::IVideoController& vidCtrl) : vidCtrl_(vidCtrl){}

	void operator()(ICommunication& communicator, Command& cmd)
	{
		uint8_t nVideos = vidCtrl_.GetVideoAmount();

		if (nVideos == 0xff)
			nVideos = 0;	// 0xff indicates an error

		uint8_t response[] = {nVideos};
		communicator.WriteSync(response, sizeof(response),10000);
	}

	uint8_t GetCode() const { return 0x11; }

	virtual ~VideoStorageProcessor(){}
private:
	Video::IVideoController& vidCtrl_;
};

} /* namespace ESP32 */

#endif /* INC_ESP32_COMMANDPROCESSORS_VIDEOSTORAGEPROCESSOR_H_ */
