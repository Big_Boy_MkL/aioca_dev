/*
 * SettingsProcessor.h
 *
 *  Created on: 12 Oct 2020
 *      Author: SYFO
 */

#ifndef INC_ESP32_COMMANDPROCESSORS_SETTINGSPROCESSOR_H_
#define INC_ESP32_COMMANDPROCESSORS_SETTINGSPROCESSOR_H_

#include <ESP32/CommandProcessors/ICommandProcessor.h>
#include <stdio.h>

#include <Configuration/Configuration.h>
#include <User/Account.h>
#include <Storage/SettingsTypes.h>

namespace ESP32 {

class SettingsProcessor : public ICommandProcessor {
public:
	SettingsProcessor(){
		account_ = &Configuration::g_CurrentAccount;
	}

	void operator()(ICommunication& communicator, Command& cmd)
	{
		if (cmd.rw){
			// Get the setting value if Read-command
			try  {
				/* Check if setting value is valid */
				if (cmd.arguments[0] > k_MaxSettingNumber) return;

				// Retrieve the setting from the current account
				uint8_t setVal = account_->GetSetting(cmd.arguments[0]);

				// Respond
				char response[1] = {setVal};
				communicator.WriteSync((uint8_t*)response, 1,10000);

			} catch (...){
				return;		// Invalid setting
			}

		} else {
			// Set the setting value and return the new value
			uint8_t setNumber 	= cmd.arguments[0];
			uint8_t setValue 	= cmd.arguments[1];

			// Set the actual setting (will just return if the settings number is invalid)
			account_->SetSetting(setNumber,setValue);		// Set local setting

			//TODO: This should be signaled since the SDCard could be in use!
			account_->UpdateSettings();						// Update on storage unit

			// Respond
			try {
				char response[1] = {setValue};
				communicator.WriteSync((uint8_t*)response, 1,10000);
			} catch (...){
				return;		// Error in communicator
			}
		}
	}

	uint8_t GetCode() const { return 0x10; }

	virtual ~SettingsProcessor(){}
private:
	User::Account* account_;
};

} /* namespace ESP32 */

#endif /* INC_ESP32_COMMANDPROCESSORS_SETTINGSPROCESSOR_H_ */
