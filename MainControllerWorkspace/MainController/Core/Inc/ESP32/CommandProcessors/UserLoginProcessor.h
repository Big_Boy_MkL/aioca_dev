/*
 * UserLoginProcessor.h
 *
 *  Created on: Nov 18, 2020
 *      Author: SYFO
 */

#ifndef INC_ESP32_COMMANDPROCESSORS_USERLOGINPROCESSOR_H_
#define INC_ESP32_COMMANDPROCESSORS_USERLOGINPROCESSOR_H_

#include <ESP32/CommandProcessors/ICommandProcessor.h>
#include <User/Account.h>
#include <Storage/SettingsTypes.h>
#include <Configuration/Configuration.h>

namespace ESP32 {

class UserLoginProcessor: public ICommandProcessor {
public:
	UserLoginProcessor() {
		memset(userIDBuffer_, 0, k_UserIDSize);
	}

	void operator()(ICommunication& communicator, Command& cmd)
	{
		// Read userID
		memcpy(userIDBuffer_, cmd.arguments, k_UserIDSize);
		userIDBuffer_[k_UserIDSize] = '\0';	// add terminating null

		uint8_t response = k_AckCharacter;

		try {
			Configuration::g_CurrentAccount.SetUserID((char*)userIDBuffer_);
		} catch (...) {
			response = k_NakCharacter;
		}

		// Respond
		communicator.WriteSync(&response, 1, 0xffff);
	}

	uint8_t GetCode() const { return 0x45; }

	virtual ~UserLoginProcessor(){}
private:
	uint8_t userIDBuffer_[k_UserIDSize+1];
};

} /* namespace ESP32 */

#endif /* INC_ESP32_COMMANDPROCESSORS_USERLOGINPROCESSOR_H_ */
