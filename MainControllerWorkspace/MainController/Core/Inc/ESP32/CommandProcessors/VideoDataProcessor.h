/*
 * VideoDateProcessor.h
 *
 *  Created on: 13 Oct 2020
 *      Author: SYFO
 */

#ifndef INC_ESP32_COMMANDPROCESSORS_VIDEODATAPROCESSOR_H_
#define INC_ESP32_COMMANDPROCESSORS_VIDEODATAPROCESSOR_H_

#include <ESP32/CommandProcessors/ICommandProcessor.h>
#include <ESP32/ESP32ProtocolConstants.h>
#include <Video/IVideoController.h>
#include <Video/Types.h>

namespace ESP32 {

static constexpr uint8_t k_blockSize = 59;

class VideoDataProcessor : public ICommandProcessor {
public:
	VideoDataProcessor(Video::IVideoController& vidCtrl) : vidCtrl_(vidCtrl){
	}

	void operator()(ICommunication& communicator, Command& cmd)
	{
		uint8_t vNumber = cmd.arguments[0];
		uint8_t blockSize = k_blockSize; 		// Use static blocksize for now  (cmd.arguments[1] for dynamic);

		// Lookup video information file
		Video::VideoInfoFile videoInfo;
		videoInfo = vidCtrl_.GetVideoInformation(vNumber);

		if (videoInfo.size == 0)
			return; 	// Send error message!

		SendVideoHeader(videoInfo,communicator);

		// Determine start frame
		int index = videoInfo.startidx - 1;

		if (videoInfo.startidx==videoInfo.n_images)
			index = -1;				// dangerous uhh oh no lul

		// Loop through images
		while(true){
			index = (index + 1) % (videoInfo.n_images + 1);

			// Send image
			SendImage(vNumber, index, communicator, blockSize);

			//TODO: This will send an extra image in case we dont start at 0
			if (index == videoInfo.n_images - 1)
				break;
		}

		// Send fin
		SendFin(communicator, blockSize);
	}

	void SendImage(uint32_t vidNumber, uint32_t imageNumber, ICommunication& communicator, uint32_t blockSize){
		// Load image
		uint32_t imgSize = vidCtrl_.GetStoredImage(vidNumber, imageNumber, jpegBuffer_, k_jpegMaxSize);

		txBuffer_[3] = imgSize>>24;
		txBuffer_[2] = imgSize>>16;
		txBuffer_[1] = imgSize>>8;
		txBuffer_[0] = imgSize;

		// Send size
		try {
			communicator.WriteSync(txBuffer_, blockSize+1, 5000);
		} catch (...){return;}

		// Wait for acknowledge
		WaitForACK(communicator);

		if (imgSize<0) return;	// image not found hmm

		// Send image
		SendFile(jpegBuffer_, imgSize, blockSize, communicator, false);
	}

	bool WaitForACK(ICommunication& communicator){
		uint8_t rxBuf = 0;
		try {
			communicator.ReadSync(&rxBuf, 1, 5000);
			return rxBuf==k_AckCharacter;
		} catch (...){return false;}
	}

	void SendVideoHeader(Video::VideoInfoFile vInfo, ICommunication& communicator)
	{

		Video::uVideoInfoFile u_vinfo;
		u_vinfo.parsed = vInfo;

		while(true){
			// Transmit
			try {
				communicator.WriteSync(u_vinfo.raw, Video::k_VideoInfoFileSize, 5000);
			} catch (...){return;}

			// Wait for ACK
			if (WaitForACK(communicator)) break;
		}
	}

	void SendFile(uint8_t* dataPtr, uint32_t dataSize, int blockSize, ICommunication& communicator, bool useFin = false) {

		int left = dataSize;
		uint8_t packetSize = blockSize;
		uint8_t* dataIterator = dataPtr;
		uint8_t rxBuf = 0;

		while(left > 0){
			/* If last packet */
			if (left-blockSize < 0)
				packetSize = left;	// Truncate packetsize

			/* Create packet */
			txBuffer_[0] = packetSize;	 // Packet size
			memcpy(txBuffer_+1, dataIterator, packetSize);

			/* Transmit packet */
			try {
				communicator.WriteSync(txBuffer_, blockSize+1, 5000);	// Always send blocksize because limitations on ESP32
			} catch(...){
				return;	// Abort SPI might be fucked TODO: Throw
			}

			/* Recieve ACK/NAK */
			try {
				communicator.ReadSync(&rxBuf, 1, 5000);
			} catch (...){
				return; // Abort SPI might be fucked TODO: Throw
			}

			/* Increment iterator and decrement left */
			if (rxBuf == k_AckCharacter){
				dataIterator+=packetSize;
				left-=packetSize;
			} else {
				volatile int x = 0;	// TODO: wtf is this
			}

		}

		/* Transmission complete send fin */
		if (useFin){
			SendFin(communicator,blockSize);
		}

	}

	void SendFin(ICommunication& communicator, uint32_t blockSize)
	{
		txBuffer_[0] = k_FinCharacter;
		try {
			communicator.WriteSync(txBuffer_, blockSize+1, 5000);	// Always send blocksize because limitations on ESP32
		} catch (...){
			return;	// TODO: Throw
		}
	}

	uint8_t GetCode() const { return 0x14; }

	virtual ~VideoDataProcessor(){}
private:

	uint8_t txBuffer_[64];
	Video::IVideoController& vidCtrl_;

	static constexpr uint32_t k_jpegMaxSize = 144*174;
	uint8_t jpegBuffer_[k_jpegMaxSize];

};

} /* namespace ESP32 */

#endif /* INC_ESP32_COMMANDPROCESSORS_VIDEODATAPROCESSOR_H_ */
