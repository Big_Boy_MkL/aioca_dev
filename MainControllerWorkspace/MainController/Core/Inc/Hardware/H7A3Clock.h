/*
 * H7A3Clock.h
 *
 *  Created on: Sep 15, 2020
 *      Author: SYFO
 */

#ifndef H7A3CLOCK_H_
#define H7A3CLOCK_H_

namespace Hardware{

class H7A3Clock {
public:
	H7A3Clock();

	bool initSystemClock() const;

	virtual ~H7A3Clock();

};
}

#endif /* H7A3CLOCK_H_ */
