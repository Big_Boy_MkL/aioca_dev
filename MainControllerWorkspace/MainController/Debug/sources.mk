################################################################################
# Automatically-generated file. Do not edit!
################################################################################

ELF_SRCS := 
C_UPPER_SRCS := 
CXX_SRCS := 
C++_SRCS := 
OBJ_SRCS := 
S_SRCS := 
CC_SRCS := 
C_SRCS := 
CPP_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
OBJDUMP_LIST := 
C_UPPER_DEPS := 
S_DEPS := 
C_DEPS := 
OBJCOPY_BIN := 
CC_DEPS := 
SIZE_OUTPUT := 
C++_DEPS := 
EXECUTABLES := 
OBJS := 
CXX_DEPS := 
S_UPPER_DEPS := 
CPP_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
Core/Src/BusErrorDetection \
Core/Src/Camera \
Core/Src/Configuration \
Core/Src/ESP32 \
Core/Src/Hardware \
Core/Src/Serial \
Core/Src/Storage \
Core/Src/Thread/Events \
Core/Src/Thread \
Core/Src/User \
Core/Src \
Core/Startup \
Drivers/STM32H7xx_HAL_Driver/Src \
FATFS/App \
FATFS/Target \
Middlewares/Third_Party/FatFs/src \
Middlewares/Third_Party/FatFs/src/option \
Middlewares/Third_Party/FreeRTOS/Source/CMSIS_RTOS_V2 \
Middlewares/Third_Party/FreeRTOS/Source \
Middlewares/Third_Party/FreeRTOS/Source/portable/GCC/ARM_CM4F \
Middlewares/Third_Party/FreeRTOS/Source/portable/MemMang \
Middlewares/Third_Party/Unity \
Utilities/JPEG \

