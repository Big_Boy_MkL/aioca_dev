/*
 * OV7670.cpp
 *
 *  Created on: Sep 15, 2020
 *      Author: SYFO
 */

#include <Camera/OV7670.h>
#include <string.h>
#include "stm32h7xx_hal.h"
#include <Util/PinDefs.h>
#include <Camera/CameraSettings.h>

namespace Camera {


/* Inline methods */
inline void OV7670::ReadClockHigh()
{
	GPIOF->BSRR = FIFO_RCK_Pin;
}

inline void OV7670::ReadClockLow()
{
	GPIOF->BSRR = (uint32_t)FIFO_RCK_Pin << 16U;
}

inline void OV7670::OutputHigh()
{
	GPIOF->BSRR = FIFO_OE_Pin;
}

inline void OV7670::OutputLow()
{
	GPIOF->BSRR = (uint32_t)FIFO_OE_Pin << 16U;
}

inline void OV7670::EnableOutput()
{
	OutputLow();
	HAL_Delay(100);
	ReadClockHigh();
	HAL_Delay(100);
	ReadClockLow();
}

inline void OV7670::DisableOutput()
{
	OutputHigh();
	HAL_Delay(100);
	ReadClockHigh();
	HAL_Delay(100);
	ReadClockLow();
}

inline void OV7670::ResetWrite()
{
	GPIOF->BSRR = (uint32_t)FIFO_WRST_Pin << 16U;
	HAL_Delay(100);
	GPIOF->BSRR = FIFO_WRST_Pin;
}

inline void OV7670::ReadResetHigh(){
	GPIOF->BSRR = FIFO_RRST_Pin;
}

inline void OV7670::ReadResetLow(){
	GPIOF->BSRR = (uint32_t)FIFO_RRST_Pin << 16U;
}

inline void OV7670::ResetRead(){
	//TODO: Check if we need delay between
	ReadResetLow();
	HAL_Delay(1);
	ReadClockHigh();
	HAL_Delay(1);
	ReadClockLow();
	HAL_Delay(1);
	ReadResetHigh();
}

inline void OV7670::EnableWrite(){
	GPIOF->BSRR = (uint32_t)FIFO_WR_Pin << 16U;
	//GPIOF->BSRR = FIFO_WR_Pin;

}

inline void OV7670::DisableWrite(){
	//GPIOF->BSRR = (uint32_t)FIFO_WR_Pin << 16U;
	GPIOF->BSRR = FIFO_WR_Pin;
}

/***** Class methods ******/

OV7670::OV7670(Serial::I2C* i2c) : i2c_(i2c), rxCount_(0) {
	//i2c_->init();	// Initialize the I2C

	// Initialize data pins and VSYNC pin
	InitPins();
	DisableVSYNCInt();
	vsync_ = false;	// reset interrupt flag!

	ResetCamera();
	//NegativeVSYNC();
	//ApplySettings();

	// Reset all pins
	DisableWrite();
	ReadClockLow();
	ReadResetHigh();
	OutputLow();

	isrState = 0;

	// Set color mode
	//SetYUVMode();
	//SetRGBMode();

	// Lower clock
	//uint8_t clkrc;
	//uint8_t dblv;

	//ReadFromRegister(0x11, &clkrc, 1);
	//ReadFromRegister(0x6b, &dblv, 1);

	//uint8_t new_clkrc = clkrc & 0b11100000;	// Normal
	//uint8_t new_clkrc = clkrc | 0b00000011;	// 1

	//uint8_t new_dblv = dblv & 0b00111111;	// Normal
	//new_dblv = new_dblv | 0b01000000;		// Fastest

	// Set prescaler
	//WriteToRegister(0x11, &new_clkrc, 1);
	//WriteToRegister(0x6b, &new_dblv, 1);

}

uint8_t OV7670::ReadFIFOByte()
{
	uint32_t data;

	// Clock data out on datapins
	ReadClockHigh();

	//for(int i = 0; i < 100; i++){}	// Delay before reading data pins
	HAL_Delay(1);

	//data = GPIOE->IDR;
	data = (GPIOE->IDR & 0x3FC) >> 2;	// PE2->PE9 is datapins!

	// Disable clock again
	ReadClockLow();

	return data;
}

uint8_t OV7670::ReadVSYNC()
{
	GPIO_PinState state;
	state = HAL_GPIO_ReadPin(VSYNC_GPIO_Port, VSYNC_Pin);
	return (uint8_t)state;
}

void OV7670::WriteToRegister(uint8_t reg, uint8_t* data, uint16_t count)
{
	// Write address, register and desired data.
	i2c_->WriteToReg(DEVICE_ADDRESS, reg, data, count);

}

void OV7670::EnableVSYNCInt(){
	HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
}

void OV7670::DisableVSYNCInt(){
	HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
	HAL_NVIC_ClearPendingIRQ (EXTI9_5_IRQn);	// Clear pending interrupts
}

void OV7670::OnInterrupt(uint16_t pin)
{
	// Disable the VSYNC interrupt
	//DisableVSYNCInt();
	/*if (isrState==0){
		EnableWrite();
		isrState++;
	} else if (isrState==1){
		DisableWrite();
		isrState = 0;
	}*/

	if (pin==VSYNC_Pin)
		vsync_ = true;
}

void OV7670::CaptureImage()
{
	// Disable write and set write address to 0
	DisableWrite();
	ResetWrite();

	/* Wait for VSYNC */
	vsync_ = false;	// reset vsync flag
	isrState = 0;
	EnableVSYNCInt();
	while(!vsync_);	// Wait for next vsync

	// Enable write
	EnableWrite();

	/* Wait for second VSYNC */
	HAL_Delay(1);
	vsync_ = false;	// reset vsync flag
	while(!vsync_);	// Wait for next vsync

	// Disable write
	DisableWrite();
	DisableVSYNCInt();

	// Disable further interrupts
	HAL_Delay(1);

	// Read the fifo-stored image
	ReadImageFromFIFO();

	DisableVSYNCInt();
}

void OV7670::ReadImageFromFIFO()
{
	// Reset the read -> sets the fifo read address to 0
	ResetRead();

	unsigned int YUV_SIZE = (float)size/2.0;
	//unsigned int RGB_SIZE = size;

	// Read the fifo-stored image
	for (unsigned int i = 0; i < YUV_SIZE; i++){

		// The FIFO only supports reading 50MHz
		// YUV
		volatile uint8_t Cb0 = ReadFIFOByte();
		volatile uint8_t Y0 = ReadFIFOByte();
		volatile uint8_t Cr0 = ReadFIFOByte();
		volatile uint8_t Y1 = ReadFIFOByte();

		// RGB
		/*
		uint8_t d1 = ReadFIFOByte();
		uint8_t d2 = ReadFIFOByte();

		uint8_t r = d2 & 0b11111000;
		uint8_t g = (d2 & 0b00000111) << 5 | (d1 & 0b11100000) >> 3;
		uint8_t b = (d1 & 0b00011111) << 3;
		 */

		/*int b = (d1 & 0x1F) << 3;
		int g = (((d1 & 0xE0) >> 3) | ((d2 & 0x07) << 5));
		int r = (d2 & 0xF8);*/

		// Save all pixels
		bw_array[i+i] = Y0;
		bw_array[i+i+1] = Y1;

	}
}


void OV7670::ResetCamera()
{
	uint8_t reset = 0b10000000;
	WriteToRegister(0x12, &reset, 1);

	HAL_Delay(200);	// Wait after resetting!
}

void OV7670::ApplySettings(/*TODO: Take settings pointer!*/)
{
	ResetCamera();

	uint8_t dataBuffer = COM7_RGB | COM7_QQVGA;

	WriteToRegister(REG_COM7, &dataBuffer, 1);

	// Transfer RGB565 specific settings
	int i = 0;
	for (;;){
		// If end
		if ((ov7670_fmt_rgb565[i].reg_num == EM) && (ov7670_fmt_rgb565[i].value == EM)) {
			return;
		}

		WriteToRegister(ov7670_fmt_rgb565[i].reg_num, &ov7670_fmt_rgb565[i].value, 1);

		i++;
	}

	//NegativeVSYNC();

	// Transfer default values
	i = 0;
	for (;;){
		// If end
		if ((ov7670_default[i].reg_num == EM) && (ov7670_default[i].value == EM)) {
			return;
		}

		WriteToRegister(ov7670_default[i].reg_num, &ov7670_default[i].value, 1);

		// delay for reset command
		if ((ov7670_default[i].reg_num == REG_COM7) && (ov7670_default[i].value == COM7_RESET)) {
			HAL_Delay(200);
		}

		i++;
	}

}

void OV7670::NegativeVSYNC()
{
	uint8_t dataBuffer = COM10_VS_NEG;
	WriteToRegister(REG_COM10, &dataBuffer, 1);
}

void OV7670::SetYUVMode(){

	uint8_t reg = 0x12;
	uint8_t mode;
	ReadFromRegister(reg, &mode, 1);
	uint8_t yuv_mode = mode & 0b11111010;
	yuv_mode = yuv_mode | 0b00000010;

	WriteToRegister(reg, &yuv_mode, 1);

}

void OV7670::SetRGBMode(){

	uint8_t reg = 0x12;	// COM7
	uint8_t rgb_mode = 0b00000100;
	WriteToRegister(reg, &rgb_mode, 1);

	reg = 0x40;	// COM 15
	uint8_t mode;
	ReadFromRegister(reg, &mode, 1);
	rgb_mode = 0b11010000;	// 0-FF and RGB565
	WriteToRegister(reg, &rgb_mode, 1);

}



void OV7670::InitPins() {
	// Empty GPIO init struct
	GPIO_InitTypeDef GPIO_InitStruct = {0};

	// Enable GPIO clocks for PORTE
	__HAL_RCC_GPIOE_CLK_ENABLE();

	/*Configure GPIO pins : DATA0_Pin DATA1_Pin DATA2_Pin DATA3_Pin
						   DATA4_Pin DATA5_Pin DATA6_Pin DATA7_Pin */
	GPIO_InitStruct.Pin = DATA0_Pin|DATA1_Pin|DATA2_Pin|DATA3_Pin
						  |DATA4_Pin|DATA5_Pin|DATA6_Pin|DATA7_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

	// Enable GPIO clocks for PORTF
	__HAL_RCC_GPIOF_CLK_ENABLE();

	/*Configure GPIO pin : VSYNC_Pin */
	GPIO_InitStruct = {0};
	GPIO_InitStruct.Pin = VSYNC_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_IT_RISING;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;
	HAL_GPIO_Init(VSYNC_GPIO_Port, &GPIO_InitStruct);

	// Enable interrupts on VSYNC
	HAL_NVIC_SetPriority(EXTI9_5_IRQn, 0, 0);
	//HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);		MUST BE ENABLED EXPLICITLY

	// Enable GPIO clocks for PORTF
	__HAL_RCC_GPIOF_CLK_ENABLE();

	/*Configure GPIO pin Output Level */
	HAL_GPIO_WritePin(GPIOF, FIFO_WR_Pin|FIFO_WRST_Pin|FIFO_OE_Pin|FIFO_RCK_Pin
						  |FIFO_RRST_Pin, GPIO_PIN_RESET);

	/*Configure GPIO pins : FIFO_WR_Pin FIFO_WRST_Pin FIFO_OE_Pin FIFO_RCK_Pin
						   FIFO_RRST_Pin */
	GPIO_InitStruct.Pin = FIFO_WR_Pin|FIFO_WRST_Pin|FIFO_OE_Pin|FIFO_RCK_Pin
						  |FIFO_RRST_Pin;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_NOPULL;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
	HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

}

void OV7670::ReadFromRegister(uint8_t reg, uint8_t* data, uint16_t count)
{
	// Write address and register in write-mode
	i2c_->Write(DEVICE_ADDRESS, &reg, 1);

	// Write address and read
	i2c_->ReadFromReg(DEVICE_ADDRESS, reg, data, count);
}


OV7670::~OV7670() {
	// TODO Auto-generated destructor stub
}

} /* namespace Camera */
