import numpy as np
import cv2
import matplotlib.pyplot as plt
from PIL import Image
from enum import Enum

def ycbcr2rgb(im):
    xform = np.array([[1, 0, 1.402], [1, -0.34414, -.71414], [1, 1.772, 0]])
    rgb = im.astype(np.float)
    rgb[:,:,[1,2]] -= 128
    rgb = rgb.dot(xform.T)
    np.putmask(rgb, rgb > 255, 255)
    np.putmask(rgb, rgb < 0, 0)
    return np.uint8(rgb)

def YUV422ToYUV(yuv422_array):
    yuv = []
    i = 0
    while (i < len(yuv422_array)):
        u = yuv422_array[i]
        y0 = yuv422_array[i+1]
        v = yuv422_array[i+2]
        y1 = yuv422_array[i+3]
        
        yuv.append(y0)
        yuv.append(u)
        yuv.append(v)
        yuv.append(y1)
        yuv.append(u)
        yuv.append(v)

        i+=4
    return np.array(yuv)

np_data = np.fromfile('I0', dtype='uint8')

yuv_data = YUV422ToYUV(np_data)

height = 144
width  = 174

img = yuv_data.reshape((height,width,3))

RGBMatrix = ycbcr2rgb(img)

im = Image.fromarray(RGBMatrix)
im.save("test.jpeg")

plt.imshow(RGBMatrix)
plt.show()