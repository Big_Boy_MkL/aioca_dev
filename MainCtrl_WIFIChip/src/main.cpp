#include <WifiController.hpp>
#include <Socket.hpp>
#include <Communication.hpp>
#include <SPI.hpp>
#include <CRC.hpp>

#include "freertos/FreeRTOS.h"
#include "freertos/queue.h"
#include "freertos/task.h"
#include "nvs_flash.h"

#include <iostream>

extern "C"
{
    void app_main(void);
}

WifiController* ctrlPtr;
Validator::CRC* valPtr;
Bus::SPI* busPtr;
STM32::Communication* commPtr;
Utility::TimeStamp* timePtr;
PacketHandler* pHandlerPtr;
TCP::Socket* SocketPtr;
QueueHandle_t tcpQueue;

void app_main()
{
    // Setup internal flash storage needed in esp funtions
    ESP_ERROR_CHECK(nvs_flash_erase());
    ESP_ERROR_CHECK(nvs_flash_init());

    ctrlPtr = new WifiController();
    ctrlPtr->SetApMode("AIOCA", "12345678");
    ctrlPtr->Start();

    QueueHandle_t tcpQueue = xQueueCreate(TCPSOCKET_MSGQUEUE_SIZE, sizeof(MessageQueueData));
    valPtr = new Validator::CRC();
    busPtr = new Bus::SPI(SPI2_HOST);
    commPtr = new STM32::Communication(valPtr, busPtr);
    timePtr = new Utility::TimeStamp(TIMER_GROUP_1, TIMER_0);
    pHandlerPtr = new PacketHandler(tcpQueue, timePtr, commPtr);
    SocketPtr = new TCP::Socket(timePtr, tcpQueue, pHandlerPtr);

    SocketPtr->Start();
}