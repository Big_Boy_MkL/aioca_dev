// #include "WifiController/WifiController.hpp"
// #include "TcpSocket/Socket.hpp"
// #include "Communicaton/Communication.hpp"
// #include "BUS/SPI.hpp"
// #include "Validator/CRC.hpp"

// #include "freertos/FreeRTOS.h"
// #include "freertos/queue.h"
// #include "freertos/task.h"
// #include "nvs_flash.h"

// #include <iostream>

// extern "C"
// {
//     void app_main(void);
// }

// void testTCPAndWiFi()
// {
//     // Setup internal flash storage needed in esp funtions
//     ESP_ERROR_CHECK(nvs_flash_erase());
//     ESP_ERROR_CHECK(nvs_flash_init());

//     WifiController ctrl = WifiController();
//     ctrl.SetApMode("Test", "testtest");
//     ctrl.Start();
    
//     Validator::CRC val = Validator::CRC();
//     Bus::SPI bus = Bus::SPI(SPI2_HOST);
//     STM32::Communication comm = STM32::Communication(&val, &bus);
//     TimeStamp time = TimeStamp(TIMER_GROUP_1, TIMER_0);
//     QueueHandle_t tcpQueue = xQueueCreate(TCPSOCKET_MSGQUEUE_SIZE, sizeof(MessageQueueData));
//     QueueHandle_t dummy = xQueueCreate(TCPSOCKET_MSGQUEUE_SIZE, sizeof(MessageQueueData));
//     PacketHandler pHandler = PacketHandler(dummy, &time, &comm);
//     TCP::Socket Socket = TCP::Socket(&time, tcpQueue, &pHandler);

//     Socket.Start();

//     while (1)
//     {
//         // TCP
//         MessageQueueData data;
//         if (xQueueReceive(tcpQueue, &(data), (TickType_t)0xff) == pdPASS)
//         {
//             for (size_t i = 0; i < data.len; i++)
//             {
//                 printf("%c ", data.message[i]);
//             }
//             Socket.SendMessage(&data.sock, data.message, data.len, false);
//             Socket.SendError(&data.sock, 0x70);
//         }
//     }
// }

// void test_CommunicationBUS()
// {
//     Bus::SPI bus = Bus::SPI(SPI2_HOST);
    
//     while (1)
//     {
//         // Setup test message
//         int testLen = 5;
//         uint8_t testMsg[testLen] = {0x01, 0x02, 0x03, 0x04, 0x05};

//         // Send test message on BUS
//         printf("Test write message to communication bus\n\n");
//         bus.WriteMessage(testMsg, &testLen);
//         vTaskDelay(1000 / portTICK_PERIOD_MS);

//         // Receive test message from bus
//         printf("Test read from communication bus\n");
//         bus.ReadMessage(testMsg, &testLen);
//         printf("Message from BUS\n");
//         for (size_t i = 0; i < testLen; i++)
//         {
//             printf("%d ", testMsg[i]);
//         }
//         printf("\n");
//         vTaskDelay(1000 / portTICK_PERIOD_MS);
//     }    
// }

// void app_main()
// {
//     test_CommunicationBUS();
// }