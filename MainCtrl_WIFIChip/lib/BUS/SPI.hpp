#ifndef _SPI_H_
#define _SPI_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "driver/spi_common.h"
#include "driver/spi_master.h"

/**
 * Private Headers
 */
#include <IBUS.hpp>

namespace Bus
{
    class SPI : public IBUS
    {
    private:
        /**
         * SPI host device
         */
        spi_host_device_t hostDevice_;

        /**
         * Handle for spi device
         */
        spi_device_handle_t spiHandle_;

        /**
        * \brief Adds a device to the \ref spi_host_device_t bus. 
        *
        * \param[in] mosi_io_num        GPIO number for mosi
        * \param[in] miso_io_num        GPIO number for miso
        * \param[in] sclk_io_num        GPIO number for clock
        * \param[in] cs_io_num          GPIO number for slave select
        * \param[in] max_transfer_sz    Max number of bytes to send in one msg (MAX 64)
        */
        void AddDevice(int mosi_io_num, int miso_io_num, int sclk_io_num, int cs_io_num, int max_transfer_sz);

    public:
        SPI(spi_host_device_t device);
        ~SPI();

        /**
        * \brief Sends data with SPI. 
        *
        * \param[in] writeBuffer        Pointer to buffer containing data to be sent.
        * \param[in] length             Number of bytes to send
        */
        bool WriteMessage(const uint8_t *writeBuffer, int *length);

        /**
        * \brief Receives data with SPI. 
        *
        * \param[in] readBuffer         Pointer to uint8_t for received data
        * \param[in] length             Number of bytes to receive
        * \return                       True if CRC is okay
        */
        bool ReadMessage(uint8_t * readBuffer, int *length);
    };
} // namespace Bus

#endif