#ifndef _IBUS_H_
#define _IBUS_H_

#include <stdint.h>

namespace Bus
{
    class IBUS
    {
    public:
        virtual bool WriteMessage(const uint8_t* message, int* length) = 0;
        virtual bool ReadMessage(uint8_t* message, int* length) = 0;
    };
} // namespace bus

#endif