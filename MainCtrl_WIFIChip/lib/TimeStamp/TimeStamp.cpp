/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <TimeStamp.hpp>
#include <GlobalDefines.hpp>
namespace Utility
{
    TimeStamp::TimeStamp(timer_group_t group, timer_idx_t index) : group_(group), index_(index)
    {
        // Create configuration struct
        timer_config_t config = {
            .alarm_en = TIMER_ALARM_DIS,
            .counter_en = TIMER_PAUSE,
            .intr_type = TIMER_INTR_LEVEL,
            .counter_dir = TIMER_COUNT_UP,
            .auto_reload = TIMER_AUTORELOAD_DIS,
            .divider = 65536,
        };

        // Initilize timer and start
        ESP_ERROR_CHECK(timer_init(group, index, &config));
        ESP_ERROR_CHECK(timer_start(group, index));
    }

    /**************************************************************************************************/

    TimeStamp::~TimeStamp()
    {
    }

    /**************************************************************************************************/

    void TimeStamp::AddTimeStamp(MessageQueueData *data)
    {
        // Read timer in seconds put time into data
        ESP_ERROR_CHECK(timer_get_counter_time_sec(group_, index_, &data->Timestamp));
    }

    /**************************************************************************************************/

    bool TimeStamp::CompareTimeStamp(MessageQueueData *data)
    {
        // Read timer in seconds
        double time = 0;
        ESP_ERROR_CHECK(timer_get_counter_time_sec(group_, index_, &time));

        // Compare stored time and return
        return ((time - data->Timestamp) < TIMESTAMP_MESSAGE_TIMEOUT) ? true : false;
    }
}