#ifndef _ITIMESTAMP_H_
#define _ITIMESTAMP_H_

namespace Utility
{
    class ITimeStamp
    {
    public:
        /**
        * \brief Adds a stamped time to message. 
        *
        * \param[in] data       A \ref MessageQueueData struct containing message.
        */
        virtual void AddTimeStamp(MessageQueueData *data) = 0;

        /**
        * \brief Compares stored stamped time and current stamped time. 
        *
        * \param[in] data       A \ref MessageQueueData struct containing message.
        * \return               Returns true if timeout has not been reached. 
        */
        virtual bool CompareTimeStamp(MessageQueueData *data) = 0;
    };
}

#endif