#ifndef _IVALIDATOR_H_
#define _IVALIDATOR_H_

#include <stdint.h>

namespace Validator
{
    class IValidator
    {
    public:
        /**
        * \brief Checks if the validation on a message is valid or not. 
        *
        * \param[in] message    A uint8_t array containing the message.
        * \param[in] length     Length of the message.
        * \return               Returns true message is ok and false if error. 
        */
        virtual bool Validate(uint8_t *message, int* length) = 0;

        /**
        * \brief Calcualtes a validation for a message. 
        *
        * \param[in] message    A uint8_t array containing the message.
        * \param[in] length     Length of the message.
        * \return               uint16_t validation code. 
        */
        virtual uint16_t GetValdidationCode(uint8_t *message, int* length) = 0;
    };
} // namespace Validator

#endif