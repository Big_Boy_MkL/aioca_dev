#ifndef _ITCPSCOKET_H_
#define _ITCPSCOKET_H_

#include <stdint.h>

namespace TCP
{
    class ISocket
    {
    public:
        virtual void Start() = 0;
        virtual void Stop() = 0;
        virtual void listenSocket() = 0;
        virtual void CheckMessageReceived(int sock) = 0;
        virtual void SendMessage(int *sock, uint8_t *message, int len, bool done) = 0;
        virtual void SendError(int *sock, uint8_t error) = 0;
    };
} // namespace TCP

struct MessageQueueData
{
    uint8_t *message;
    int len;
    int sock;
    TCP::ISocket *socketPtr;
    double Timestamp;
};

#endif