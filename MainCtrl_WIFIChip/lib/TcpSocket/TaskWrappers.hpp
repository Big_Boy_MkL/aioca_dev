#ifndef _TASKWRAPPERS_H_
#define _TASKWRAPPERS_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <ISocket.hpp>
#include <PacketHandler.hpp>

struct CheckMessageData
{
    TCP::ISocket *ptr;
    int sock;
};

// Wrapper to use class method in task
inline void TCPSocketListenWrapper(void *param)
{
    static_cast<TCP::ISocket *>(param)->listenSocket();
}

inline void TCPSocketCheckMessageWrapper(void *param)
{
    CheckMessageData *data = (CheckMessageData *)param;
    (data->ptr)->CheckMessageReceived(data->sock);
    delete (data);
    vTaskDelete(NULL);
}

// Wrapper to use class method in task
inline void PacketHandlerHandleWrapper(void *param)
{
    while (1)
    {
        static_cast<PacketHandler *>(param)->Handle();
    }
}

#endif