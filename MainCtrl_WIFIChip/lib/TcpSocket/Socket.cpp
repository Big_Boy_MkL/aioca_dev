
/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */
#include "lwip/sockets.h"

/**
 * Private Headers
 */
#include <Socket.hpp>
#include <TaskWrappers.hpp>
#include <TimeStamp.hpp>
#include <GlobalDefines.hpp>

namespace TCP
{
    Socket::Socket(Utility::ITimeStamp* time, QueueHandle_t msgQueue, PacketHandler* phandler) : timeStamper_(time), messageQueue_(msgQueue), pkgHandler_(phandler)
    {
        // Set up socket ip address and port
        struct sockaddr_in dest_addr_ip4;
        dest_addr_ip4.sin_addr.s_addr = htonl(INADDR_ANY);
        dest_addr_ip4.sin_family = AF_INET;
        dest_addr_ip4.sin_port = htons(TCPSOCKET_SOCKETPORT);

        // Create a TCP socket
        listen_sock_ = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
        if (listen_sock_ < 0)
        {
            // Return if failed
            return;
        }

        // Bind socket to ip address defined above
        if (bind(listen_sock_, (struct sockaddr *)&dest_addr_ip4, sizeof(dest_addr_ip4)) != 0)
        {
            // Return if failed
            return;
        }

        // Make socket listen for incomming connections
        if (listen(listen_sock_, TCPSOCKET_MAX_CONNECTIONS) != 0)
        {
            // Return if failed
            return;
        }
    }

    /**************************************************************************************************/

    Socket::~Socket()
    {
        // Close TCP socket and delete tasks
        close(listen_sock_);

        if (listenTaskHandle_ != NULL)
        {
            vTaskDelete(listenTaskHandle_);
        }
        if (packetTaskHandle_ != NULL)
        {
            vTaskDelete(packetTaskHandle_);
        }
    }

    /**************************************************************************************************/

    void Socket::Start()
    {
        // Only start task if TCP socket has been created
        if (listen_sock_ != 0)
        {
            // Start listen and packet handler tasks
            xTaskCreate(PacketHandlerHandleWrapper, "PacketHandler", 4096, pkgHandler_, 5, packetTaskHandle_);
            xTaskCreate(TCPSocketListenWrapper, "TCP_Socket", 2048, this, 5, listenTaskHandle_);
        }
    }

    /**************************************************************************************************/

    void Socket::Stop()
    {
        // Stop listen and packet handler tasks
        vTaskDelete(listenTaskHandle_);
        vTaskDelete(packetTaskHandle_);
    }

    /**************************************************************************************************/

    void Socket::listenSocket()
    {
        while (1)
        {
            struct sockaddr source_addr;
            uint addr_len = sizeof(source_addr);

            // Accept incomming TCP connection
            int sock = accept(listen_sock_, &source_addr, &addr_len);
            if (sock > 0)
            {
                // Create data struckture for CheckMessageReceived task.
                CheckMessageData *data = new CheckMessageData{
                    .ptr = this,
                    .sock = sock,
                };

                // Create a task to check if TCP connection is being used.
                xTaskCreate(TCPSocketCheckMessageWrapper, "CheckMessageReceived Task", 2048, (void *)data, 5, NULL);
            }
        }
    }

    /**************************************************************************************************/

    void Socket::CheckMessageReceived(int sock)
    {
        // Make child socket a FD
        fd_set rfds;
        FD_SET(sock, &rfds);

        // Check if message is sent after 3 way handshake within 2 seconds
        if (select(sock + 1, &rfds, NULL, NULL, &tv_) > 0)
        {
            ReceiveMessage(&sock);
        }
        else
        {
            Shutdown(&sock);
        }
    }

    /**************************************************************************************************/

    void Socket::SendError(int *sock, uint8_t error)
    {
        // Error message
        uint8_t data[3] = {PROTOCOL_ERROR_START, error, PROTOCOL_END};

        // Send error and shutdown child.
        SendMessage(sock, data, 3, true);
    }

    /**************************************************************************************************/

    void Socket::SendMessage(int *sock, uint8_t *message, int len, bool done)
    {
        // Send message
        send(*sock, message, len, 0);
                
        // Shutdown socket if flag is set
        if (done)
        {
            Shutdown(sock);
        }
    }

    /***************************************************************************************************
     * Private methods
     ***************************************************************************************************/

    void Socket::ReceiveMessage(int *sock)
    {
        // Create a buffer for incomming msg
        uint8_t* buffer = new uint8_t[WIFI_RECEIVE_BUFFER_SIZE];

        // Receive message
        int len = recv(*sock, buffer, WIFI_RECEIVE_BUFFER_SIZE - 1, 0);

        // Create data stuckture to put into queue
        MessageQueueData data = {
            .message = buffer,
            .len = len,
            .sock = *sock,
            .socketPtr = this,
            .Timestamp = 0,
        };

        // Stamp time so timeout can be checked
        timeStamper_->AddTimeStamp(&data);

        // Put msg in queue
        if (xQueueSend(messageQueue_, (void *)&data, (TickType_t)10) != pdPASS)
        {
            // Send error
            SendError(sock, PROTOCOL_ERROR_SPIMSGQUEUEFULL);
        }
    }

    /**************************************************************************************************/

    void Socket::Shutdown(int *sock)
    {
        // Shutdown and close child socket
        shutdown(*sock, 0);
        close(*sock);
    }
}