#ifndef _ICOMMUNICATION_H_
#define _ICOMMUNICATION_H_

#include <stdint.h>

namespace STM32
{
    class ICommunication
    {       
    public:
        virtual void WriteMessage(uint8_t *message, int *length) = 0;
        virtual void ReceiveMessage(uint8_t *message, int *length) = 0;
    };
} // namespace STM32

#endif