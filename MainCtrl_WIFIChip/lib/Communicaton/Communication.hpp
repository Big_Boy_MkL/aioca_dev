#ifndef _COMMUNICATION_H_
#define _COMMUNICATION_H_

/**
 * C++ Libs
 */

/**
 * FreeRTOS Libs
 */

/**
 * ESP Libs
 */

/**
 * Private Headers
 */
#include <GlobalDefines.hpp>
#include <CRC.hpp>
#include <SPI.hpp>
#include <ICommunication.hpp>

namespace STM32
{
    class Communication : public ICommunication
    {
    public:
        Communication(Validator::IValidator *val, Bus::IBUS *bus);
        ~Communication();

        
        /**
        * \brief Writes a message to the serial interface bus. 
        *
        * \param[in] message    A uint8_t array containing the message.
        * \param[in] length     The length of the message. 
        */
        void WriteMessage(uint8_t *message, int *length);

        /**
        * \brief Receives a message on the serial bus. This method will also
        *        retry the transmisson if the validator cant validate the message. 
        *
        * \param[in] message    A uint8_t array for the received message.
        * \param[in] length     The length that should be received. 
        */
        void ReceiveMessage(uint8_t *message, int *length);

    private:
        /**
         * Pointer to the validator class
         */
        Validator::IValidator *val_;

        /**
         * Pointer to the serial bus class
         */
        Bus::IBUS* bus_;

        /**
         * Pointer to the origial message. It is used if the message
         * needs to be retransmittet. 
         */
        uint8_t* orgMsg_;

        /**
         * Pointer to the length of the origial message. It is used if the message
         * needs to be retransmittet. 
         */
        int* orgLen_;

        /**
         * \brief Sends a acknowledge on the serial interface. 
         */
        void SendACK();

        /**
         * \brief Sends a not acknowledge on the serial interface. 
         */
        void SendNAK();
        
    };
} // namespace STM32

#endif