#ifndef _TEST_TIMEAMP_H_
#define _TEST_TIMEAMP_H_

#include <unity.h>
#include <GlobalDefines.hpp>

#include <Socket.hpp>
#include <TaskWrappers.hpp>
#include <Communication.hpp>
#include <SPI.hpp>
#include <CRC.hpp>
#include <PacketHandler.hpp>
#include "Fake_Classes/fake_Socket.hpp"

#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/queue.h>

QueueHandle_t tcpQueue;
TCP::fake_socket* fakeSocketPtr;
Utility::TimeStamp* timePtr;
PacketHandler* pHandlerPtr;

void test_LoginCommand_Success()
{
    int len = 32;
    uint8_t* message = new uint8_t[len] {0x01, 0x00, 0x45, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 3;
    uint8_t expectedMessage[expectedLen] {0x01, 0x06, 0x04};
    
    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expectedMessage, fakeSocketPtr->ReturnMessage(), expectedLen);
}

void test_LoginCommand_Fail()
{
    int len = 32;
    uint8_t* message = new uint8_t[len] {0x01, 0x00, 0x45, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 3;
    uint8_t expectedMessage[expectedLen] {0x01, 0x15, 0x04};
    
    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expectedMessage, fakeSocketPtr->ReturnMessage(), expectedLen);
}

void test_ReadVersionNumber()
{
    int len = 4;
    uint8_t* message = new uint8_t[len] {0x01, 0xff, 0x13, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 5;
    uint8_t expectedMessage[expectedLen] {0x01, 0x01, 0x00, 0x00, 0x04};
    
    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expectedMessage, fakeSocketPtr->ReturnMessage(), expectedLen);
}

void test_WriteSettingCommand()
{
    int len = 6;
    uint8_t* message = new uint8_t[len] {0x01, 0x00, 0x10, 0x02, 0x10, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 3;
    uint8_t expectedMessage[expectedLen] {0x01, 0x10, 0x04};
    
    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expectedMessage, fakeSocketPtr->ReturnMessage(), expectedLen);    
}

void test_ReadSettingCommand()
{
    int len = 5;
    uint8_t* message = new uint8_t[len] {0x01, 0xff, 0x10, 0x02, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 3;
    uint8_t expectedMessage[expectedLen] {0x01, 0x10, 0x04};
    
    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expectedMessage, fakeSocketPtr->ReturnMessage(), expectedLen);    
}

void test_ReadVideoStorageCommand()
{
    int len = 4;
    uint8_t* message = new uint8_t[len] {0x01, 0xff, 0x11, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 3;
        
    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL(0x01, fakeSocketPtr->ReturnMessage()[0]);
    TEST_ASSERT_GREATER_OR_EQUAL(0x00, fakeSocketPtr->ReturnMessage()[1]);    
    TEST_ASSERT_EQUAL(0x04, fakeSocketPtr->ReturnMessage()[2]);    
}

void test_ReadVideoInfoCommand()
{
    int len = 5;
    uint8_t* message = new uint8_t[len] {0x01, 0xff, 0x12, 0xff, 0x04};

    MessageQueueData data = {
        .message = message,
        .len = len,
        .sock = 0,
        .socketPtr = fakeSocketPtr,
        .Timestamp = 0,
    };

    timePtr->AddTimeStamp(&data);

    xQueueSend(tcpQueue, (void *)&data, (TickType_t)10);

    pHandlerPtr->Handle();
    
    int expectedLen = 19;
    uint8_t expectedMessage[expectedLen] {0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x04};

    TEST_ASSERT_EQUAL(expectedLen, fakeSocketPtr->ReturnLen());
    TEST_ASSERT_EQUAL_UINT8_ARRAY(expectedMessage, fakeSocketPtr->ReturnMessage(), expectedLen);    
}

void test_Intergration()
{
    tcpQueue = xQueueCreate(TCPSOCKET_MSGQUEUE_SIZE, sizeof(MessageQueueData));
    TCP::fake_socket fakeSocket = TCP::fake_socket();
    Validator::CRC val = Validator::CRC();
    Bus::SPI bus = Bus::SPI(SPI2_HOST);
    STM32::Communication comm = STM32::Communication(&val, &bus);
    Utility::TimeStamp time = Utility::TimeStamp(TIMER_GROUP_1, TIMER_0);
    PacketHandler pHandler = PacketHandler(tcpQueue, &time, &comm);
    fakeSocketPtr = &fakeSocket;
    pHandlerPtr = &pHandler;
    timePtr = &time;

    RUN_TEST(test_LoginCommand_Fail);
    RUN_TEST(test_LoginCommand_Success);
    RUN_TEST(test_ReadVersionNumber);
    RUN_TEST(test_WriteSettingCommand);
    RUN_TEST(test_ReadSettingCommand);
    RUN_TEST(test_ReadVideoStorageCommand);
    RUN_TEST(test_ReadVideoInfoCommand);
}

#endif