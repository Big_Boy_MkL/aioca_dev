#ifndef _FAKE_TEST_BUS_H_
#define _FAKE_TEST_BUS_H_

#include <IBUS.hpp>

class fake_Test_BUS : public Bus::IBUS
{
private:
    uint8_t WriteMessageCalled = 0;
    bool WriteMessageReturnValue[10] = {false};

    uint8_t ReadMessageCalled = 0;
    bool ReadMessageReturnValue[10] = {false};
public:

    void setGetWriteMessageCodeReturn(bool val, uint8_t index)
    {
        WriteMessageReturnValue[index] = val;
    }

    bool WriteMessage(const uint8_t *message, int *length)
    {
        WriteMessageCalled++;
        return WriteMessageReturnValue[WriteMessageCalled-1];
    }

    uint8_t WriteMessageCodeNumOfCalls()
    {
        return WriteMessageCalled;
    }

    void setGetReadMessageCodeReturn(bool val, uint8_t index)
    {
        ReadMessageReturnValue[index] = val;
    }

    bool ReadMessage(uint8_t *message, int *length)
    {
        ReadMessageCalled++;
        return ReadMessageReturnValue[ReadMessageCalled-1];
    }

    uint8_t ReadMessageCodeNumOfCalls()
    {
        return ReadMessageCalled;
    }
};

#endif