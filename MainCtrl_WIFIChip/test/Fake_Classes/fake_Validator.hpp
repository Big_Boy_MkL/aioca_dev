#ifndef _FAKE_TEST_VALIDATOR_H_
#define _FAKE_TEST_VALIDATOR_H_

#include <IValidator.hpp>

class fake_Test_Validator : public Validator::IValidator
{
private:
    uint8_t ValidateCalled = 0;
    bool ValidateReturnValue[10] = {false};

    uint8_t GetValdidationCodeCalled = 0;
    uint16_t GetValdidationCodeReturnValue[10] = {0};
public:
    void setValidateReturn(bool val, uint8_t index)
    {
        ValidateReturnValue[index] = val;
    }

    bool Validate(uint8_t *message, int *length)
    {
        ValidateCalled++;
        return ValidateReturnValue[ValidateCalled-1];
    }

    uint8_t ValidateNumOfCalls()
    {
        return ValidateCalled;
    }

    void setGetValdidationCodeReturn(uint16_t val, uint8_t index)
    {
        GetValdidationCodeReturnValue[index] = val;
    }

    uint16_t GetValdidationCode(uint8_t *message, int *length)
    {
        GetValdidationCodeCalled++;
        return GetValdidationCodeReturnValue[GetValdidationCodeCalled-1];
    }

    uint8_t ValdidationCodeNumOfCalls()
    {
        return GetValdidationCodeCalled;
    }
};

#endif